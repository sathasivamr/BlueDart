import { HTTPUtility } from './utils/http-utility';

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { RequestMethod } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class CustomBackEndService {




    hostUrl: any = "http://log.tigerjump.in/tjhindu";
    postionList: any = [];
    // This is trial key. Original key is yet to be provided by NVE.


    constructor(public http: Http, private httpUtil: HTTPUtility) { }


    getAllUnit(): Promise<any> {
        return this.httpUtil.sendRequest(RequestMethod.Post, null, null, null, true).then(resp => {
            return Promise.resolve<any>(resp.json());
        }).catch((err) => {
            return Promise.reject<any>('');
        });

    }

    createRoute(payload: any) {
        let URL = this.hostUrl + '/route';
        return this.httpUtil.sendRequest(RequestMethod.Post, URL, payload, null, true).then(resp => {
            return Promise.resolve<any>(resp.json());
        }).catch((err) => {
            return Promise.reject<any>('');
        });
    }
    getRoute(userName) {
        let URL = this.hostUrl + '/route/getby?user=' + userName;
        return this.httpUtil.sendRequest(RequestMethod.Get, URL, null, null, true).then(resp => {
            return Promise.resolve<any>(resp.json());
        }).catch((err) => {
            return Promise.reject<any>('');
        });
    }
    deleteRoute() {
        return this.httpUtil.sendRequest(RequestMethod.Get, null, null, null, true).then(resp => {
            return Promise.resolve<any>(resp.json());
        }).catch((err) => {
            return Promise.reject<any>('');
        });
    }
    editRoute() {
        return this.httpUtil.sendRequest(RequestMethod.Get, null, null, null, true).then(resp => {
            return Promise.resolve<any>(resp.json());
        }).catch((err) => {
            return Promise.reject<any>('');
        });
    }
}