/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WialonApiServiceService } from './wialon-api-service';

describe('WialonApiServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WialonApiServiceService]
    });
  });

  it('should ...', inject([WialonApiServiceService], (service: WialonApiServiceService) => {
    expect(service).toBeTruthy();
  }));
});
