import { HTTPUtility } from '../utils/http-utility';

import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { RequestMethod } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
@Injectable()
export class WialonApiServiceService {



  hostUrl: any = "http://wapi.tigerjump.in/ajax.html?";
  postionList: any = [];
  // This is trial key. Original key is yet to be provided by NVE.


  constructor(public http: Http, private httpUtil: HTTPUtility) { }


  getAllUnit(): Promise<any> {
    return this.getSID().then((res) => {
      const getunit: any = this.hostUrl + `svc=core/search_items&sid=` + res + `&params={"spec":{"itemsType":"avl_unit",
      "propName":"sys_name","propValueMask":"*","sortType":"","propType":""},"force":1,"flags":4611686018427387903,"from":0,"to":0}`;
      return this.httpUtil.sendRequest(RequestMethod.Post, getunit, 'HOME', null, true).then(resp => {
        return Promise.resolve<any>(resp.json());
      }).catch((err) => {
        return Promise.reject<any>('');
      });
    }).catch((err) => {
      return Promise.reject<any>('');
    });
  }


  getSID(): Promise<any> {
    // if (typeof (Storage) !== "undefined") {
    //   // Store
    //   return Promise.resolve(localStorage.getItem("sid"));
    // }
    // return null;
    //0dae7f5dce0299bbdee2b12fce2880e3D0D778F8C637FE8436A05EE922975492A9DD54FD  - SC
    //93e857ce0e401b2583740b9a77b40658E4B45CCF71DCB63F60A427E2676BD141DE96FC27  - IT
    const jwt = '"' + '0dae7f5dce0299bbdee2b12fce2880e3D0D778F8C637FE8436A05EE922975492A9DD54FD' + '"';
    const siteURL: any = this.hostUrl + "svc=token/login&params={\"token\":" + jwt + "}";
    return this.httpUtil.sendRequest(RequestMethod.Post, siteURL, null).then((res) => {
      console.log(res);
      return Promise.resolve<any>(res.json().eid);
    }).catch((err) => {
      return Promise.reject('');
    });


  }
  getAvlGroupDetails() {
    return this.getSID().then((res) => {
      const groupURL: any = this.hostUrl + `svc=core/search_items&sid=` + res + `&params={"spec":{"itemsType":"avl_unit_group",
            "propName":"sys_name","propValueMask":"*","sortType":"","propType":""},"force":1,"flags":4611686018427387903,"from":0,"to":0}`;
      return this.httpUtil.sendRequest(RequestMethod.Post, groupURL, 'HOME', null, true).then(resp => {

        return Promise.resolve<any>(resp.json());
      }).catch((err) => {
        return Promise.reject<any>('Error Occured While Fetching Group Details');
      });
    }).catch((err) => {
      return Promise.reject<any>('Invalid Session Id');
    });
  }

  getResourceDetails() {
    return this.getSID().then((res) => {
      const resourceURL: any = this.hostUrl + `svc=core/search_items&sid=` + res + `&params={"spec":{"itemsType":"avl_resource",
            "propName":"sys_name","propValueMask":"*","sortType":"","propType":""},"force":1,"flags":4611686018427387903,"from":0,"to":0}`;
      return this.httpUtil.sendRequest(RequestMethod.Post, resourceURL, 'HOME', null, true).then(resp => {

        return Promise.resolve<any>(resp.json());
      }).catch((err) => {
        return Promise.reject<any>('Error Occure While Fetching Group Details');
      });
    }).catch((err) => {
      return Promise.reject<any>('Invalid Session Id');
    });
  }
  // create the fuel report
  //fromDate, todayDate, groupIDOrUnitID, userID, templateID
  genrateReprot(fromDate, todayDate, groupIDOrUnitID, userID, templateID) {
    return this.getSID().then((res) => {
      const clearReportURI = 'http://wapi.tigerjump.in/ajax.html?svc=report/cleanup_result&sid=' +
        res + '&params={"params":[{"svc":"report/cleanup_result","params":{}},{"svc":"report/cleanup_result","params":{}},{"svc":"report/get_report_data","params":{"itemId": ' + userID + ' ,"col":["' + templateID + '"],"flags":0}}],"flags":0}';

      const reportHeader = 'http://wapi.tigerjump.in/ajax.html?svc=report/exec_report&params={"reportResourceId": ' + userID + ' , "reportTemplateId": 16,"reportObjectId":' + groupIDOrUnitID + ' ,"reportObjectSecId":0,"interval":{"from": ' + fromDate + ' ,"to": ' + todayDate + ' ,"flags":16777216},"reportObjectIdList":[]}&sid=' + res;
      return this.httpUtil.sendRequest(RequestMethod.Post, clearReportURI).then((clearReport) => {
        console.log('Clean', clearReport);
        return this.httpUtil.sendRequest(RequestMethod.Post, reportHeader).then((reportHeaderRes) => {

          if (reportHeaderRes.json().reportResult.tables.length > 1) {
            let arrayOfTable = [];
            let reportType = ''
            let count = 0;
            reportHeaderRes.json().reportResult.tables.forEach(element => {
              if (element.name === "unit_messages_tracing") {
                reportType = 'travellog';
              }
              arrayOfTable.push(element.name);
            });

            if (reportType == 'travellog') {
              return Promise.resolve<any>(this.travelLogReport(reportHeaderRes, res, arrayOfTable, reportType));
            } else {
              return Promise.resolve<any>(false);
            }


          } else {
            return Promise.resolve<any>(false);
          }
        }).catch((err) => {

        });
      }).catch((err) => {

      });
    }).catch((err) => {

    });
  }

  travelLogReport(reportHeaderRes, res, arrayOfTable, reportType): Promise<any> {


    let table1 = arrayOfTable.indexOf("unit_trips") >= 0 ? arrayOfTable.indexOf("unit_trips") : '';
    let table2 = arrayOfTable.indexOf("unit_messages_tracing") >= 0 ? arrayOfTable.indexOf("unit_messages_tracing") : '';
    let table3 = arrayOfTable.indexOf("unit_fillings") >= 0 ? arrayOfTable.indexOf("unit_fillings") : '';

    let row1 = reportHeaderRes.json().reportResult.tables[table1] && reportHeaderRes.json().reportResult.tables[table1].rows ? reportHeaderRes.json().reportResult.tables[table1].rows : '';
    let row2 = reportHeaderRes.json().reportResult.tables[table2] && reportHeaderRes.json().reportResult.tables[table2].rows ? reportHeaderRes.json().reportResult.tables[table2].rows : '';
    let row3 = reportHeaderRes.json().reportResult.tables[table3] && reportHeaderRes.json().reportResult.tables[table3].rows ? reportHeaderRes.json().reportResult.tables[table3].rows : ''
    const reportTable1: any = 'http://wapi.tigerjump.in/ajax.html?svc=report/select_result_rows&sid=' + res + '&params={"tableIndex":' + table1 + ',"config":{"type":"range","data":{"from":0,"to":' + row1 + ',"level":1}}}'
    const reportTable2: any = 'http://wapi.tigerjump.in/ajax.html?svc=report/select_result_rows&sid=' + res + '&params={"tableIndex":' + table2 + ',"config":{"type":"range","data":{"from":0,"to":' + row2 + ',"level":1}}}'
    const reportTable3: any = 'http://wapi.tigerjump.in/ajax.html?svc=report/select_result_rows&sid=' + res + '&params={"tableIndex":' + table3 + ',"config":{"type":"range","data":{"from":0,"to":' + row3 + ',"level":1}}}'
    return Promise.all([this.httpUtil.sendRequest(RequestMethod.Post, reportTable1), this.httpUtil.sendRequest(RequestMethod.Post, reportTable2), this.httpUtil.sendRequest(RequestMethod.Post, reportTable3)]).then((reportTableResult) => {
      console.log(reportTableResult);
      const result = {
        'header': reportHeaderRes.json(), firstReport: reportTableResult[0].json(), secondReport: reportTableResult[1].json(),
        thirdReport: reportTableResult[2].json(), arrayOfTable: arrayOfTable, reportType: reportType,
        tripIndex: table1, msgIndex: table2, fuelIndex: table3
      };
      return Promise.resolve<any>(result);
    }).catch((err) => {
      console.log('err', err);
    });
  }

  getUnitLocation(latlan) {
    return this.getSID().then((res) => {
      const getunit: any = this.hostUrl + `svc=core/duplicate&sid=` + res + '&params={\"operateAs\": \"\"  ,\"continueCurrentSession\":' + true + ',\"checkService\":\ "\"  }';
      return this.httpUtil.sendRequest(RequestMethod.Post, getunit, 'RE', null, true).then(resp => {
        const url = "https://geocode-maps.wialon.com/hst-api.wialon.com/gis_geocode?flags=961544192&city_radius=10&dist_from_unit=5&txt_dist=km from&coords=" + JSON.stringify(latlan) + "&uid=" + resp.json().user.id + "&sid=" + res;
        return this.httpUtil.sendRequest(RequestMethod.Post, url, 'RE', null, true).then(result => {

          return Promise.resolve<any>(result.json());
        }).catch((err) => {
          return Promise.reject<any>('');
        });
      }).catch((err) => {
        return Promise.reject<any>('');
      });
    }).catch((err) => {
      return Promise.reject<any>('');
    });
  }


  getFuelGeoLocation(): Promise<any> {
    return this.getSID().then((res) => {
      const getunit: any = this.hostUrl + `svc=core/search_items&sid=` + res + `&params={"spec":{"itemsType":"avl_resource",
      "propName":"sys_name","propValueMask":"*","sortType":"","propType":""},"force":1,"flags":4109,"from":0,"to":0}`;
      return this.httpUtil.sendRequest(RequestMethod.Post, getunit, 'Fuel', null, true).then(resp => {
        return Promise.resolve<any>(resp.json());
      }).catch((err) => {
        return Promise.reject<any>('');
      });
    }).catch((err) => {
      return Promise.reject<any>('');
    });
  }


  getReportTripDontDelete(resourceID, tempID) {

    return this.getSID().then((res) => {
      const getunit: any = this.hostUrl + `svc=report/get_report_data&sid=` + res + `&params={"itemId":` + resourceID + `,"col":[` + tempID + `],"flags":0}`;
      return this.httpUtil.sendRequest(RequestMethod.Post, getunit, 'Fuel', null, true).then(resp => {
        return Promise.resolve<any>(resp.json());
      }).catch((err) => {
        return Promise.reject<any>('');
      });
    }).catch((err) => {
      return Promise.reject<any>('');
    });
  }
}





