import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { WialonApiServiceService } from 'app/service/wialon/wialon-api-service';
import { empty } from 'rxjs/Observer';
import * as moment from 'moment';

declare var require;
declare const $: any;
var jsPDF = require('jspdf');
require('jspdf-autotable');
declare var AmCharts;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {



  fromDate = new Date(moment(this.fromDate).subtract(1, 'days').format());
  toDate = new Date()
  templateDetails: Array<any> = [];
  messageTable: any;
  tripTable: any;
  fuelTable: any;
  getAllUnit: any;
  selectedunit: any;
  selectedPos: any
  selectedTemplate: any;
  selectedtempPos: any;
  header: any;
  stats: any = [];
  chart: any;
  showResult: boolean = false;
  showSpinner: boolean = false;
  travelLogReportArray: any = [];
  userDetail: any;
  neededInfo: any = {
    isFuelInfoPresent: false,
    isLogReport: false,
    isStat: false,
    msgIndex: 0,
    tripIndex: 0,
    fuelindex: 0
  };
  settings = {
    columns: {
      A_dateTime: {
        title: 'Date',
        filter: false
      },
      B_location: {
        title: 'Location',
        filter: false
      },
      C_speed: {
        title: 'Speed Km/h',
        filter: false
      },
      D_kmTraveled: {
        title: 'Total KM',
        filter: false
      },
      E_driver: {
        title: 'Driver',
        filter: false
      }

    },
    noDataMessage: '',
    actions: {
      add: false,
      edit: false,
      delete: false,
      columnTitle: ''
    },
    pager: {
      display: true,
      perPage: 8
    }

  };
  constructor(private wialonApi: WialonApiServiceService) {

  }
  ngOnInit() {

    this.chart = AmCharts.makeChart("chartdiv", {
      "type": "pie",
      "theme": "light",
      "hideCredits": true,
      "dataProvider": [
        {
          "country": "Idle",
          "litres": 1.2,
          "color": "#85B665"
        }, {
          "country": "Running",
          "litres": 2,
          "color": "#CA484B"
        }],
      "valueField": "litres",
      "titleField": "country",
      "colorField": "color",
      "balloon": {
        "fixedPosition": true
      },
      "export": {
        "enabled": false
      }
    });
    document.getElementById("sample").style.display = 'none';
    Promise.all([this.wialonApi.getResourceDetails(), this.wialonApi.getAllUnit()]).then((result) => {
      console.log(result);
      this.userDetail = result[0].items[0]
      const obj = result[0].items[0].rep;
      this.getAllUnit = result[1].items;
      this.templateDetails = Object.keys(obj).map(function (key) { return obj[key]; });
      console.log(this.templateDetails, this.getAllUnit);
      // this.templateDetails.forEach(element => {
      // });


    }).catch((err) => {

    });

  }
  // // const result = {
  //   'header': reportHeaderRes.json(), firstReport: reportTableResult[0].json(), secondReport: reportTableResult[1].json(),
  //   thirdReport: reportTableResult[2].json(), arrayOfTable: arrayOfTable, reportType: reportType
  // };
  submit() {
    if (this.selectedPos && this.selectedTemplate && this.selectedtempPos && this.selectedunit) {
      let diff = moment(this.fromDate).diff(this.toDate);
      console.log(diff)
      const selectedDate = moment(this.fromDate).unix();
      const nextDate = moment(this.toDate).unix();
      //  const nextDate = moment(this.toDate).add(1, 'day').unix();
      // this.previousFromDate = moment(this.reportStartDate).format("DD/MM/YYYY");
      // this.previousToDate = moment(this.reportEndDate).format("DD/MM/YYYY");
      if (nextDate > selectedDate) {



        this.travelLogReportArray = [];
        this.showSpinner = true;
        document.getElementById("sample").style.display = 'none';
        console.log(this.userDetail);
        const groupID = this.selectedunit.id;
        const userID = this.userDetail.id;
        const templateID = this.selectedTemplate.id;
        this.wialonApi.genrateReprot(selectedDate, nextDate, groupID, userID, templateID).then((res) => {
          console.log(res);
          if (res.reportType === "travellog") {
            this.messageTable = res.secondReport;
            this.tripTable = res.firstReport;
            this.fuelTable = res.thirdReport;
            this.header = res.header.reportResult.tables;
            this.neededInfo.msgIndex = res.msgIndex;
            this.neededInfo.tripIndex = res.tripIndex;
            this.neededInfo.tripIndex = res.tripIndex;

            this.neededInfo.isFuelInfoPresent = this.fuelTable.error ? false : true;
            this.neededInfo.isLogReport = this.tripTable.error && this.messageTable.error ? false : true;
            this.stats = {
              startTime: '', endTime: '', unit: '',
              runninghour: '', totalKm: '', idealduration: '', maxSpeed: '', avgSpeed: '', vioCount: ''
            }
            res.header.reportResult.stats.forEach(element => {

              if (element[0] === "Unit") {
                this.stats.unit = element[1];
              } else if (element[0] === "Interval beginning") {
                this.stats.startTime = moment(element[1]).utcOffset("+05:30").format()



              } else if (element[0] === "Interval end") {
                this.stats.endTime = moment(element[1]).utcOffset("+05:30").format()
              } else if (element[0] === "Move time") {
                this.stats.runninghour = element[1];
              } else if (element[0] === "Mileage in trips") {
                this.stats.totalKm = element[1];
              } else if (element[0] === "Parking time") {
                this.stats.idealduration = element[1];
              } else if (element[0] === "Max speed in trips") {
                this.stats.maxSpeed = element[1];
              } else if (element[0] === "Average speed in trips") {
                this.stats.avgSpeed = element[1];
              } else if (element[0] === "Violations count") {
                this.stats.vioCount = element[1];
              }

            });
            let that = this;
            this.showSpinner = false;
            this.showResult = true;



            document.getElementById("sample").style.display = 'block';



            this.doProcessingLog();
          }
        }).catch((err) => {

        });
      } else {
        window.alert('select the valid start and end date');
      }
    } else {
      window.alert('Selec the Correct Unit and template');
    }
  }

  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2 - lon1);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180);
  }

  doProcessingLog() {


    this.chart.dataProvider = [];
    let that = this;


    let number1 = Math.round(moment.duration(that.stats.idealduration).asHours())
    let number2 = Math.round(moment.duration(that.stats.runninghour).asHours())
    this.chart.dataProvider.push({
      "country": "Idle",
      "litres": number1,
      "color": "#85B665"
    }, {
        "country": "Running",
        "litres": number2,
        "color": "#CA484B"
      })
    this.chart.validateData();



    let tripRowIndex = 0;


    let tripStart = true;
    let letStartCount = true;

    let tripRow;
    let dateTime;
    let location
    let driver;

    let totalEntry = 0;
    let firstTime = 0;
    let differnceTime = 0;
    let lastIndex = 0;
    let timeInterVal = 120;

    // tripRow = { isnewTrip: false, parking: '', dateTime: dateTime, location: location, speed: speed, kmTraveled: '', driver: driver }

    try {
      let tripRowIndex = 0;
      this.tripTable.forEach(trip => {
        let totalSpeed = 0;
        let tripStartTime = this.tripTable[tripRowIndex].c[this.getMsgIndex(this.neededInfo.tripIndex, 'Beginning')].v
        let tripEndTime = this.tripTable[tripRowIndex].c[this.getMsgIndex(this.neededInfo.tripIndex, 'End')].v;

        let msgRowIndex = 0;
        let differnceTime: any = 0;

        let isParking = {
          dateFor: moment.unix(Number(this.tripTable[tripRowIndex].c[this.getMsgIndex(this.neededInfo.tripIndex, 'Beginning')].v)).format("DD/MM/YYYY"),
          parking: this.tripTable[tripRowIndex].c[this.getMsgIndex(this.neededInfo.tripIndex, 'Off-time')],
          rlocation: location
        }
        this.travelLogReportArray.push(isParking);
        this.messageTable.forEach(element => {
          let curentValue = this.messageTable[msgRowIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Time')].v;
          if (curentValue >= tripStartTime && curentValue <= tripEndTime) {
            let distance: any = 0;
            if (msgRowIndex > 0) {
              let lat1 = this.messageTable[msgRowIndex - 1].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Coordinates')].y;
              let lon1 = this.messageTable[msgRowIndex - 1].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Coordinates')].x;
              let lat2 = this.messageTable[msgRowIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Coordinates')].y;
              let lon2 = this.messageTable[msgRowIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Coordinates')].x;

              let curentValue1 = this.messageTable[msgRowIndex - 1].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Time')].v;
              let differnce = moment.duration(moment.unix(curentValue).diff(moment.unix(curentValue1)));
              differnceTime = differnceTime + Math.abs(differnce.asSeconds());
              distance = this.getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2);
            } else if (msgRowIndex === 0) {
              let lat1 = this.messageTable[msgRowIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Coordinates')].y;
              let lon1 = this.messageTable[msgRowIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Coordinates')].x;
              let lat2 = this.messageTable[msgRowIndex + 1].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Coordinates')].y;
              let lon2 = this.messageTable[msgRowIndex + 1].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Coordinates')].x;

              let curentValue2 = this.messageTable[msgRowIndex + 1].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Time')].v;

              let differnce = moment.duration(moment.unix(curentValue2).diff(moment.unix(curentValue)));
              differnceTime = differnceTime + Math.abs(differnce.asSeconds());

              distance = this.getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2);
            }

            if (!isNaN(distance)) {
              firstTime = distance + firstTime;
            } else {
              console.log(distance, firstTime)
            }
            if (differnceTime >= timeInterVal) {
              differnceTime = 0;
              let speed = this.messageTable[msgRowIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Speed')];
              totalSpeed = Math.round(Number(speed.replace('km/h', '').trim()));
              speed = totalSpeed > 0 ? speed : '1 km/h';
              dateTime = moment.unix(Number(this.messageTable[msgRowIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Time')].v)).format("HH:mm:ss");
              location = this.getLocation(this.messageTable[msgRowIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Location')].t);
              driver = this.messageTable[msgRowIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Driver')];
              tripRow = { A_dateTime: dateTime, B_location: location, C_speed: speed, D_kmTraveled: firstTime.toFixed(2), E_driver: driver }
              this.travelLogReportArray.push(tripRow);

            }
            lastIndex = msgRowIndex;
          }
          msgRowIndex++;
        });

        if (differnceTime > 0) {
          differnceTime = 0;
          let speed = this.messageTable[lastIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Speed')];
          totalSpeed = Math.round(Number(speed.replace('km/h', '').trim()));
          speed = totalSpeed > 0 ? speed : '1 km/h';
          dateTime = moment.unix(Number(this.messageTable[lastIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Time')].v)).format("HH:mm:ss");
          location = this.getLocation(this.messageTable[lastIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Location')].t);
          driver = this.messageTable[lastIndex].c[this.getMsgIndex(this.neededInfo.msgIndex, 'Driver')];
          tripRow = { A_dateTime: dateTime, B_location: location, C_speed: speed, D_kmTraveled: firstTime.toFixed(2), E_driver: driver }
          this.travelLogReportArray.push(tripRow);

        }
        tripRowIndex++;
      });



    } catch (err) {
      console.log(err);
    }


  }

  getLocation(location: any) {
    if (location.indexOf("from") > 0) {
      let arr = location.split(',');

      let res = arr[arr.length - 1].split("from")
      return res[1] + " (" + res[0] + ")";

    } else {
      let state = location.replace('Tamil Nadu,', '');
      let state1 = state.replace('Tamil Nadu', '');
      let country = state1.replace('India,', '');
      let country1 = country.replace('India', '');

      return country1;
    }

  }

  getTripIndex() {

  }

  getMsgIndex(index, textString) {
    return this.header[index].header.indexOf(textString)
  }



  donwloadPdf(isDownload) {

    const kmReportColumn = ['Time', 'City', 'Speed', 'Total Km', 'Driver'];
    const state = ['Time', 'City', 'Speed', 'Total Km', 'Driver'];

    const name = 'ShanthiChiken'
    const rows = this.travelLogReportArray;
    const col = kmReportColumn;
    const output = rows.map(function (obj) {
      return Object.keys(obj).sort().map(function (key) {
        return obj[key];
      });
    });
    // let mode = 'landscape';
    // var doc = new jsPDF(mode, "mm", "a4");


    var currentpage = 0;

    let footer = function (data) {
      if (currentpage < doc.internal.getNumberOfPages()) {
        doc.setFontSize(10);
        doc.setFontStyle('normal');
        doc.text("Copyright (c) Tigerjump All Rights Reserved.", data.settings.margin.left, doc.internal.pageSize.height - 5);
        doc.setFontSize(14);
        doc.setFontSize(12);
        doc.setTextColor(0);
        doc.setFontStyle('bold');
        doc.text('Shanthichicken' + " - Travel Log", 40, 30);

        currentpage = doc.internal.getNumberOfPages();
      }
    };




    var doc = new jsPDF('p', 'pt');


    var data = output;
    let that = this;

    let rowIn = 0;
    doc.autoTable(col, data, {
      theme: 'grid',
      margin: { horizontal: 8 },
      startY: 60,
      addPageContent: footer,
      styles: { overflow: 'linebreak' },
      drawRow: function (row, data) {
        // Colspan

        doc.setFontStyle('bold');
        doc.setFontSize(10);
        rowIn = that.travelLogReportArray[row.index];
        if (that.travelLogReportArray[row.index] && that.travelLogReportArray[row.index].parking) {
          if (row.index === 0) {

            return false;
          }
          doc.setTextColor(200, 0, 0);
          doc.rect(data.settings.margin.left, row.y, data.table.width, 20, 'S');
          doc.autoTableText(that.travelLogReportArray[row.index].dateFor + " - " +
            that.travelLogReportArray[row.index].parking + " - " +
            that.travelLogReportArray[row.index].rlocation, data.settings.margin.left + data.table.width / 2, row.y + row.height / 2, {
              halign: 'center',
              valign: 'middle'
            });
          data.cursor.y += 20;
          return false;
        }
        if (row.index % 5 === 0) {
          var posY = row.y + row.height * 6 + data.settings.margin.bottom;
          if (posY > doc.internal.pageSize.height) {
            data.addPage();
          }
        }
      },

      drawCell: function (cell, data) {
        // Rowspan

        if (data.column.dataKey === 'id') {
          if (data.row.index % 5 === 0) {
            doc.rect(cell.x, cell.y, data.table.width, cell.height * 5, 'S');
            doc.autoTableText(data.row.index / 5 + 1 + '', cell.x + cell.width / 2, cell.y + cell.height * 5 / 2, {
              halign: 'center',
              valign: 'middle'
            });
          }
          return false;

        }
      }
    });



 

      if (!isDownload) {
        doc.autoPrint();


        window.open(doc.output('bloburl'), '_blank');
      } else {

        doc.save(name + '.pdf');

      }
    


  }
  selectedUnit(event: any) {

    if (event.target.value !== "-1") {
      this.selectedunit = this.getAllUnit[event.target.value];
      this.selectedPos = event.target.value;
      console.log(this.selectedunit, this.selectedPos);
    } else {
      this.selectedunit = '';
      this.selectedPos = '';
      window.alert('Select the unit')
    }

  }

  selectedTemp(event: any) {

    if (event.target.value !== "-1") {
      this.selectedTemplate = this.templateDetails[event.target.value];
      this.selectedtempPos = event.target.value;
      console.log(this.selectedTemplate, this.selectedtempPos);
    } else {
      this.selectedTemplate = '';
      this.selectedtempPos = '';
      window.alert('Select the unit')
    }

  }
  downloadStat(isDownload) {
    var pdf = new jsPDF('p', 'pt');
    var options = {
      'margin': 1
    };
    pdf.addHTML($("#printDiv"), 2, 140, options, function () {

    });

    pdf.setFontSize(10);
    pdf.setFontStyle('normal');
    //doc.text("Copyright (c) tigerjump All Rights Reserved.", 40, 100 - 5);
    pdf.setFontSize(14);
    pdf.setFontSize(12);
    pdf.setTextColor(0);
    pdf.setFontStyle('bold');
    pdf.text('Shanthichicken' + " - Travel Log", 40, 30);
    setTimeout(function () {

      if (!isDownload) {

        pdf.autoPrint();
        window.open(pdf.output('bloburl'), '_blank');

      } else {
        pdf.save(name + 'state.pdf')


      }
    }, 2000);
  }


}
